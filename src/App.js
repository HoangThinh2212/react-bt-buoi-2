import "./App.css";
import MainIndex from "./Component-BT/MainIndex";

function App() {
  return (
    <div className="App" style={{backgroundImage:"url(./glassesImage/background.jpg)" , objectFit:"fill"}}>
      <MainIndex />
    </div>
  );
}

export default App;
