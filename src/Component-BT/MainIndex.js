import React, { Component } from "react";
import { dataGlass } from "./dataGlasses";
import DetailGlass from "./DetailGlass";
import ListGlass from "./ListGlass";

export default class MainIndex extends Component {
  state = {
    glassArr: dataGlass,
    detail: dataGlass[0],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };
  render() {
    return (
      <div className="container">
        <div className="title py-4 mx-auto" style={{color:"coral", fontSize:"16px"}}>Try Glasses App Online</div>
        <div className="row justify-content-center align-items-center">
          <img
            className="col-5 mr-5 position-relative"
            style={{ width: "400px", height: "400px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <img
            className="col-5 ml-5"
            style={{ width: "400px", height: "400px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
        </div>

        <ListGlass
          handleChangeDetail={this.handleChangeDetail}
          glassArr={this.state.glassArr}
        />

        <DetailGlass detail={this.state.detail} />
      </div>
    );
  }
}
