import React, { Component } from "react";

export default class ItemGlass extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className="col-2 py-3">
        <div>
          <img
            style={{ width: "100%" }}
            src={url}
            alt=""
            onClick={() => {
              this.props.handleViewDetail(this.props.data);
            }}
          />
        </div>
      </div>
    );
  }
}
