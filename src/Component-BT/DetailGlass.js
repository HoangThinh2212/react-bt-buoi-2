import React, { Component } from "react";

export default class DetailGlass extends Component {
  render() {
    return (
      <div>
        <div
          className="position-absolute"
          style={{
            top: "61.2%",
            left: "10%",
            width: "34.2%",
            height: "25%",
            backgroundColor: "#E0FFFF",
            textAlign: "left",
          }}
        >
          <img
            style={{
              marginTop: "-300px",
              marginLeft: "23.5%",
              width: "52%",
              height: "40%%",
              opacity: "0.5",
            }}
            src={this.props.detail.url}
            alt=""
          />
          <p style={{ color: "#4682B4", fontSize: "20px" }}>
            {this.props.detail.name}
          </p>
          <p style={{ fontSize: "16px", fontWeight: "bold" }}>
            {this.props.detail.desc}
          </p>
        </div>
      </div>
    );
  }
}
